subscription_id="00a10da9-4060-4d8b-9531-98edb20e131a"
client_id="95159452-a117-4423-8b1e-c4c03f689f25"
client_secret="64f02760-6c16-42d9-b470-88b8d3c8c1a2"
tenant_id="5f24eb1e-6706-4b30-8c2f-6a169dd5d076"
name={
  "0"="virtualnetwork_1"
  "1"="virtualnetwork_2"
}

resource_group_name ="azurerm_resource_group.newresourcegroup.name"
address_space       =["10.0.0.0/16"]
name1={
  "0"="subnet_1"
  "1"="subnet_2"
}
name2="public_ip1"
name3={
  "0"="network_interface1"
  "1"="network_interface2"
  "2"="network_interface3"
  "3"="network_interface4"
}
name4={
  "0"="testconfiguration1"
  "1"="testconfiguration2"
  "2"="testconfiguration3"
  "3"="testconfiguration4"
}

name5="acceptanceTestPublicIp2"
name6="acceptanceTestNetworkInterface2"
name7="testconfiguration2"
name8={
  "0"="security_group_1"
  "1"="security_group_2"
}
name9={
  "0"="storagedevopsgtm1"
  "1"="storagedevopsgtm2"
}

name10={
  "0"="VM_1"
  "1"="VM_2"
  "2"="VM_3"
  "3"="VM_4"
}
name11={
  "0"="OsDisk_1"
  "1"="OsDisk_2"
  "2"="OsDisk_3"
  "3"="OsDisk_4"
}
name12="myVM2iewzr"
name13="myOsDisk2"
name14="SSH"
name15="HTTP"
name16={
  "0"="Resource_1"
  "1"="Test_Javier_2"
}
location1={
  "0"="East US"
  "1"="East US2"
}
location2={
  "0"="East US"
  "1"="East US2"
}

adress1="10.0.1.0/24"
location3="East US2"
adress2="dynamic"
adress3="dynamic"
location4="East US"
adress4="dynamic"
adress5="dynamic"
location5={
  "0"="East US"
  "1"="East US2"
}
account1="LRS"
account2="Standard"
location6={
  "0"="East US"
  "1"="East US2"
}
vm1="Standard_DS1_v2"
caching1="ReadWrite"
creat1= "FromImage"
managed1="Premium_LRS"
publi1="Canonical"
offer1="UbuntuServer"
sku1="16.04.0-LTS"
version="latest"
computer_name1="myVM1zuer834"
admin_user1="stage"
path1="/home/stage/.ssh/authorized_keys"
key_data1="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5G4vIkS5dGpeo2W/WWlfX2X3JEjM0p3EJC6LG57ZPJqZ1MBl0NWTTvs/HfoYW3GRCwSykjMj1pG8VpxFvEVRcIiz1G40JRDwK6MzLhtl952NQcB7OuNZzzOSAx1+PEgBkiMUxkr9ixcZ9Z4CVtP/aft3qs2FGr2cqzC2uOU2fpSCPLLCJqTHOAOPE3BqIC8UhS3OPGxQtwr/alpmQ/4HVryPbnSwwVG6yjBjCIW+/wF1C3gd+dCPGrRbBstJa2vwezs+/8qnWGDEpZxVPBNPgRC84plluJrf2YjzbOYXIpe2XAVPAD9MJOFw3zyJbhtIDtluU5o6zpF/F+mGttW81"
enable1="true"
location7="East US"
vm2="Standard_DS1_v2"
caching2="ReadWrite"
creat2="FromImage"
managed2="Premium_LRS"
publi2="Canonical"
offer2="UbuntuServer"
sku2="16.04.0-LTS"
computer_name2="myVM2iewzr"
admin_username1="stage"
admin_password1="pA1!assword"
enabled2="true"
location8="West US"
location9="East US2"
